# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from assignment.custom_fields import (CaseInsensitiveCharField,
        CaseInsensitiveEmailField)

from assignment.custom_models import BaseModel
from User.models import User
from .constants import *

''' Class to Describe Activity; An activity can have types which are 
    specified as activity choice; An activity choice has M2M relationship
    with User
'''
class Activity(BaseModel):
    LIKE = 0
    DISLIKE = 1
    SHARE = 2
    COMMENT = 3
    ACTIVITY_CHOICE  = ((LIKE, 'Liked by Peer'),
                        (DISLIKE, 'Disliked by Peer'),
                        (SHARE, 'Shared by Peer'),
                        (COMMENT, 'Commented by Peer'))
    activity_mode = models.SmallIntegerField(default=LIKE,
            choices=ACTIVITY_CHOICE)
    activity_content = models.TextField(blank=True, null=True)
    taken_by = models.ManyToManyField(User, blank=False)

    def delete_activity(self):
        self.status = INACTIVE
        self.save()
        return self

    def __str__(self):
        return "Id -- {} -- content  -- {} -- Status {}".format(
            self.pk, self.activity_content or '', self.get_activity_mode_display())



''' Class to contain post information; Only text input as a userpost
    is allowed
'''
class Post(BaseModel):
    content = models.TextField(blank=False, null=False)
    posted_by = models.ForeignKey(User, related_name='author')
    activity_map = models.ManyToManyField(Activity, blank=True,
            related_name='activity')

    def delete_post(self):
        self.status = INACTIVE
        activity = self.activity_map.all().update(status=INACTIVE)
        self.save()
        return self

    def __str__(self):
        return "Id -- {} --  Posted_By -- {} -- Status {}".format(
            self.pk, self.posted_by.name, self.get_status_display())


''' Notification Description which has a FK Mapping to User
    and its related post
'''
class Notification(BaseModel):
    MAIL = 0
    NOTIF_MODE = ((MAIL, 'Notification through Email!'),)
    name = models.CharField(max_length=255, null=True, blank=True)
    mode = models.SmallIntegerField(default=MAIL,
            choices=NOTIF_MODE)
    notif_content = models.TextField(blank=False, null=False)
    related_post = models.ForeignKey(Post, related_name='post')
    related_user = models.ForeignKey(User, related_name='notif_user',
            null=True)
    
    def __str__(self):
        return "{} - {}".format(self.pk, self.mode)
