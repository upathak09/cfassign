import json

from rest_framework import serializers

from .models import (Notification, Post, Activity)

from .tasks import activity_notification

from User.models import User

class NotificationSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    notif_mode = serializers.SerializerMethodField()

    def get_status(self, instance):
        return instance.get_status_display()

    def get_notif_mode(self, instance):
        return instance.get_mode_display()

    class Meta:
        model = Notification
        fields = ('status', 'notif_mode', 'related_post', 'notif_content',
            'related_user', 'id')


class ActivitySerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    taken_by = serializers.PrimaryKeyRelatedField(
            many=True, read_only=True)
    activity_display = serializers.SerializerMethodField()

    def get_status(self, instance):
        return instance.get_status_display()

    def get_activity_display(self, instance):
        return instance.get_activity_mode_display()

    def create(self, validated_data):
        instance = Activity.objects.create(**validated_data)
        return instance

    def update(self, instance, validated_data):
        taken_by = json.loads(self.context.pop('taken_by'))
        post_id = self.context.pop('post')
        post = Post.objects.get(pk=post_id)
        post.activity_map.add(instance)
        user = User.objects.filter(pk__in=taken_by)
        for field in [k for k in validated_data]:
            setattr(instance, field, validated_data[field])
        instance.taken_by.add(*user)
        instance.save()
        activity_notification.delay(post_id, user[0].pk,
            instance.activity_mode, user[0].name)
        return instance

    class Meta:
        model = Activity
        fields = ('status', 'taken_by', 'activity_content', 'activity_mode',
                'activity_display', 'id')


class PostSerializer(serializers.ModelSerializer):

    status = serializers.SerializerMethodField()
    activity_map = serializers.PrimaryKeyRelatedField(
                many=True, read_only=True)

    def get_status(self, instance):
        return instance.get_status_display()

    class Meta:
        model = Post
        fields = ('content', 'posted_by', 'activity_map', 'status', 'id')