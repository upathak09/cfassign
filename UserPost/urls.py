from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from .views import (PostView, ActivityView)

router = DefaultRouter()
router.register(r'userpost', PostView, base_name='userpost')
router.register(r'useractivity', ActivityView, base_name='useractivity')

urlpatterns = [
    url(r'^', include(router.urls)),

]
