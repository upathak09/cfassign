# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager

from django.core.validators import validate_email

from assignment.custom_fields import (CaseInsensitiveCharField,
		CaseInsensitiveEmailField)

from assignment.custom_models import BaseModel


# Create your models here.

class LoggedDevice(models.Model):
    name = models.CharField('Logged Device',
    		max_length=10, default="website")

    def __str__(self):
        return self.name



class User(AbstractBaseUser, BaseModel):
    """
        An abstract base class implementing a fully featured User model with
        admin-compliant permissions.

        email is required. Other fields are optional.
    """
    SEX = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    mobile = CaseInsensitiveCharField('Mobile',
    		max_length=20, null=True, blank=True)
    email = CaseInsensitiveEmailField('Email', blank=False, null=False,
    		unique=True, validators=[validate_email])
    name = models.CharField('Name', max_length=128, blank=False, null=False)
    is_email_verified = models.BooleanField('Email Verified', default=False)
    is_phone_verified = models.BooleanField('Phone Verified', default=False)
    logged_device = models.ForeignKey(LoggedDevice,
    		null=True, blank=True, on_delete=models.SET_NULL)
    anniversary = models.DateField('Anniversary', null=True, blank=True)
    description = models.TextField('Description', null=True, blank=True)
    sex = models.SmallIntegerField(choices=SEX, null=True, blank=True)
    objects = BaseUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __str__(self):
        return self.email